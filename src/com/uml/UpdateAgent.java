package com.uml;

import android.content.Context;
import android.util.Log;

import com.umeng.update.UmengDownloadListener;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import com.umeng.update.UpdateStatus;

/**
 * 更新升级代理
 * 
 * @author xiaohua
 * 
 */
public class UpdateAgent {

	private static final String TAG = UpdateAgent.class.getSimpleName();

	static {
		// 禁止集成检测功能
		UmengUpdateAgent.setUpdateCheckConfig(false);
	}

	public static final void update(Context context, final OnUpdateListener listener) {
		UmengUpdateAgent.update(context);
		UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {

			@Override
			public void onUpdateReturned(int code, UpdateResponse updateInfo) {
				switch (code) {
				case UpdateStatus.NoneWifi:
					Log.d(TAG, "没有wifi");
					break;
				case UpdateStatus.Yes:
					Log.d(TAG, "有新版本");
					break;
				case UpdateStatus.No:
					if (listener != null)
						listener.noneNewer();
					Log.d(TAG, "目前是最新的");
					break;
				case UpdateStatus.Timeout:
					if (listener != null)
						listener.onTimeout();
					Log.d(TAG, "超时");
					break;
				default:
					Log.w(TAG, "" + code);
					break;
				}
			}
		});

		UmengUpdateAgent.setDownloadListener(new UmengDownloadListener() {

			@Override
			public void OnDownloadStart() {
				Log.w(TAG, "download start");
			}

			@Override
			public void OnDownloadUpdate(int progress) {
				Log.w(TAG, "download progress : " + progress + "%");
			}

			@Override
			public void OnDownloadEnd(int result, String file) {
				Log.w(TAG, "download file path : " + file);
			}
		});
	}

	/**
	 * 检查是否有更新文件
	 * 
	 * @param context
	 */
	public static final void update(Context context) {
		UmengUpdateAgent.update(context);
		UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {

			@Override
			public void onUpdateReturned(int code, UpdateResponse updateInfo) {
				switch (code) {
				case UpdateStatus.NoneWifi:
					Log.d(TAG, "没有wifi");
					break;
				case UpdateStatus.Yes:
					Log.d(TAG, "有新版本");
					break;
				case UpdateStatus.No:
					Log.d(TAG, "目前是最新的");
					break;
				case UpdateStatus.Timeout:
					Log.d(TAG, "超时");
					break;
				default:
					Log.w(TAG, "" + code);
					break;
				}
			}
		});

		UmengUpdateAgent.setDownloadListener(new UmengDownloadListener() {

			@Override
			public void OnDownloadStart() {
				Log.w(TAG, "download start");
			}

			@Override
			public void OnDownloadUpdate(int progress) {
				Log.w(TAG, "download progress : " + progress + "%");
			}

			@Override
			public void OnDownloadEnd(int result, String file) {
				Log.w(TAG, "download file path : " + file);
			}
		});
	}
}
