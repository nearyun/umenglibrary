package com.uml;

public interface OnUpdateListener {
	/**
	 * 目前是最新
	 */
	public void noneNewer();

	/**
	 * 超时
	 */
	public void onTimeout();
}
